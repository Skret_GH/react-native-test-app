import { StyleSheet } from 'react-native';

export const generalStyles = StyleSheet.create({
  appBackground: {
    backgroundColor: '#4f4f4f',
  },

  textColor: {
    color: '#dadbe2',
  },

  spinnerColor: {
    color: '#3ba077',
  }
});
