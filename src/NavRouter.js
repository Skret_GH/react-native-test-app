import React from 'react';
import { connect } from 'react-redux';
import { Router, Scene, Stack } from 'react-native-router-flux';
import { StyleSheet } from 'react-native';
import PhotosList from './components/PhotosList';
import FullScreenPhoto from './components/FullScreenPhoto';
import { generalStyles } from './appStyles';
import ErrorPage from './components/utils/ErrorPage';

import { ERROR, FULL_SCREEN_PHOTO, PHOTOS_LIST } from './routes';
import { clearFullscreenPhoto, stopLoading } from './actions/photoActions';

class NavRouter extends React.Component {

  render() {
    return (
      <Router sceneStyle={generalStyles.appBackground}>
        <Stack key='root'>
          <Scene
            key={PHOTOS_LIST}
            component={PhotosList}
            title='Unsplash'
            navigationBarStyle={styles.navBar}
            titleStyle={styles.title}
            initial
          />
          <Scene
            key={FULL_SCREEN_PHOTO}
            component={FullScreenPhoto}
            onExit={this.props.clearFullscreenPhoto}
            hideNavBar
          />
          <Scene
            key={ERROR}
            component={ErrorPage}
            onExit={this.props.stopLoading}
            hideNavBar
          />
        </Stack>
      </Router>
    );
  }


}

const styles = StyleSheet.create({
  navBar: {
    backgroundColor: '#4b9a78',
  },

  title: {
    ...generalStyles.textColor,
  }
});

export default connect(null, { clearFullscreenPhoto, stopLoading })(NavRouter);
