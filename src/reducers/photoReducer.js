import {
  LOAD_PHOTOS_PAGE,
  LOAD_FULLSCREEN_PHOTO,
  PHOTOS_LOADING_STARTED,
  PHOTOS_LOADING_FINISHED,
  CLEAR_FULLSCREEN_PHOTO,
} from '../actions/actionTypes';

const initialState = {
  page: 0,
  list: [],
  currentIndex: undefined,
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PHOTOS_PAGE:
      return {
        ...state,
        page: action.payload.page,
        list: action.payload.list,
        loading: false,
      };
    case LOAD_FULLSCREEN_PHOTO:
      return {
        ...state,
        ...action.payload,
      };
    case CLEAR_FULLSCREEN_PHOTO:
      return {
        ...state,
        currentIndex: undefined,
      };
    case PHOTOS_LOADING_STARTED:
      return {
        ...state,
        loading: true,
      };
    case PHOTOS_LOADING_FINISHED:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
