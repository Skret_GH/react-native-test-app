import { combineReducers } from 'redux';
import photoReducer from './photoReducer';

const initialState = {};

const appReducer = combineReducers({
  photos: photoReducer,
});

export default (state = initialState, action) => appReducer(state, action);
