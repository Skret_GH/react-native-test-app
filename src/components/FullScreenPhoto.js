import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-image-progress';
import { Actions } from 'react-native-router-flux';
import { generalStyles } from '../appStyles';
import { clearFullscreenPhoto } from '../actions/photoActions';
import LoadingSpinner from './utils/LoadingSpinner';
import { ERROR } from '../routes';

const ERROR_MESSAGE = 'Image loading error';
const IMAGE_SIZE = 'large';

class FullScreenPhoto extends Component {

  onErrorHandler = () =>
    Actions.replace(ERROR,
      { message: ERROR_MESSAGE });

  render() {
    const photos = this.props.list;
    const currentPhoto = photos[this.props.currentIndex];

    if (!currentPhoto) {
      return <LoadingSpinner />;
    }

    return (
      <View>
        <Image
          indicator={() => <ActivityIndicator
            size={IMAGE_SIZE}
            color={generalStyles.spinnerColor.color}
          />}
          onError={this.onErrorHandler}
          source={{ uri: currentPhoto.uriFull }}
          style={styles.fullScreenImage}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fullScreenImage: {
    ...generalStyles.appBackground,
    width: '100%',
    height: '100%',
  },
});

const mapStateToProps = (state) => {
  const { currentIndex, list } = state.photos;
  return { currentIndex, list };
};

export default connect(mapStateToProps, { clearFullscreenPhoto })(FullScreenPhoto);
