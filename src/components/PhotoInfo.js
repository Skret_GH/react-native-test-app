import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { generalStyles } from '../appStyles';

const Props = {
  uri: '',
  userName: '',
  description: '',
  onPress: () => {},
};

class PhotoInfo extends React.Component<Props> {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    const {
      uri,
      userName,
      description,
      onPress
    } = this.props;

    return (
        <TouchableOpacity
          style={styles.infoContainer}
          onPress={onPress}
        >
          <View styles={styles.imageContainer}>
            <Image
              source={{ uri }}
              style={styles.img}
            />
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.authorText}>
              {userName}
            </Text>
            <Text style={styles.descriptionText}>
              {description || userName}
            </Text>
          </View>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 4,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 4,
    borderWidth: 0.5,
    backgroundColor: '#2e3030',
  },

  textContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    paddingLeft: 10,
  },

  authorText: {
    ...generalStyles.textColor,
    fontWeight: 'bold',
    fontSize: 15,
  },

  descriptionText: {
    ...generalStyles.textColor,
    fontSize: 12,
    paddingLeft: 4,
  },

  imageContainer: {
    flex: 1,
  },

  img: {
    width: 80,
    height: 90,
  },
});

export default PhotoInfo;
