import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { generalStyles } from '../../appStyles';
import { PHOTOS_LIST } from '../../routes';

const DEFAULT_BUTTON_TITLE = 'Photos';

const errorPage = (props) => {
  const onPressHandler = () => {
    if (props.onPress) {
      props.onPress();
      return;
    }
    Actions.replace(PHOTOS_LIST);
  };

  return (
    <View style={styles.errorContainer}>
      <Text style={styles.errorTitle}>
        Error :(
      </Text>
      <Text style={styles.errorMassage}>
        {props.message}
      </Text>
      <Button
        onPress={onPressHandler}
        title={props.title || DEFAULT_BUTTON_TITLE}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  errorContainer: {
    ...generalStyles.appBackground,
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },

  errorTitle: {
    ...generalStyles.textColor,
    fontWeight: 'bold',
    fontSize: 30,
  },

  errorMassage: {
    ...generalStyles.textColor,
    fontSize: 14,
  }
});

export default errorPage;
