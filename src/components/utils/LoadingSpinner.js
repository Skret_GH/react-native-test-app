import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { generalStyles } from '../../appStyles';

const loadingSpinner = () => (
  <View style={styles.loadingContainer}>
    <ActivityIndicator
      size='large'
      color={generalStyles.spinnerColor.color}
    />
    <Text style={styles.loading}>Loading...</Text>
  </View>
);

const styles = StyleSheet.create({
  loadingContainer: {
    ...generalStyles.appBackground,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  loading: {
    ...generalStyles.textColor,
  },
});

export default loadingSpinner;
