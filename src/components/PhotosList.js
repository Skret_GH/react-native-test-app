import { FlatList, RefreshControl, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadPhotosPage, loadFullscreenPhoto } from '../actions/photoActions';
import PhotoInfo from './PhotoInfo';
import { generalStyles } from '../appStyles';
import LoadingSpinner from './utils/LoadingSpinner';


class PhotosList extends Component {


  componentDidMount() {
    const { list } = this.props.photos;
    if (!list || list.length === 0) {
      this.props.loadPhotosPage();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { list, loading } = this.props.photos;

    if (this.ensureListFullness(list)
      && !(nextProps.photos.loading)
      && !loading) {
      this.props.loadPhotosPage();
    }
  }

  ensureListFullness = (list) => !list || list.length === 0;

  renderInfo = ({ item, index }) =>
    (
      <PhotoInfo
        uri={item.uriSmall}
        userName={item.userName}
        description={item.description}
        onPress={() => this.props.loadFullscreenPhoto(index)}
      />
    );

  render() {
    const { page, list, loading } = this.props.photos;

    if (list.length === 0) {
      return (
        <LoadingSpinner />
      );
    }
    
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          style={styles.listContainer}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              progressBackgroundColor={generalStyles.appBackground.backgroundColor}
              colors={[generalStyles.spinnerColor.color]}
            />}
          onEndReached={() => this.props.loadPhotosPage(page + 1)}
          data={[...list]}
          keyExtractor={item => item.id}
          renderItem={this.renderInfo}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    ...generalStyles.appBackground,
    width: '100%',
    flex: 1,
    padding: 0,
  },

  loadingContainer: {
    ...generalStyles.appBackground,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  loading: {
    ...generalStyles.textColor,
  },

  listBottomLoading: {
    height: 60,
    justifyContent: 'center',
  }
});

const mapStateToProps = (state) => {
  const { photos } = state;
  return { photos };
};

export default connect(mapStateToProps, { loadPhotosPage, loadFullscreenPhoto })(PhotosList);
