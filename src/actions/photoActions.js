import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import {
  LOAD_PHOTOS_PAGE,
  LOAD_FULLSCREEN_PHOTO,
  PHOTOS_LOADING_STARTED,
  PHOTOS_LOADING_FINISHED, CLEAR_FULLSCREEN_PHOTO
} from './actionTypes';
import { ERROR } from '../routes';

const PHOTO_BASE_URL = 'https://api.unsplash.com';
const PHOTO_RELATIVE = '/photos';
const HEADER_TYPE = 'Authorization';
const TOKEN_PREFIX = 'Client-ID';
const TOKEN = '896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043';

axios.defaults.baseURL = PHOTO_BASE_URL;
axios.defaults.headers.common[HEADER_TYPE] = `${TOKEN_PREFIX} ${TOKEN}`;

export const loadPhotosPage = (numOfPage = 1) => (dispatch, getState) => {
  const state = getState();
  if (state.photos.loading) return;

  const list = [...state.photos.list];
  dispatch({ type: PHOTOS_LOADING_STARTED });

  axios.get(PHOTO_RELATIVE, {
    params: {
      page: numOfPage,
    }
  })
    .then(response => {
      const extractedData = extractPhotoInfo(response.data);
      dispatch({
        type: LOAD_PHOTOS_PAGE,
        payload: {
          list: [...list, ...extractedData],
          page: numOfPage,
        }
      });
    })
    .catch(error => {
      if (Actions.currentScene !== ERROR) {
        Actions.error({ message: error.toString() });
        dispatch({ type: PHOTOS_LOADING_FINISHED });
      }
    });
};

export const loadFullscreenPhoto = (photoIndex) => (dispatch, getState) => {
  const state = getState();
  if (state.photos.currentIndex === photoIndex) {
    return;
  }

  dispatch({
    type: LOAD_FULLSCREEN_PHOTO,
    payload: {
      currentIndex: photoIndex,
    },
  });
  Actions.fullScreenPhoto();
};

export const clearFullscreenPhoto = () => (dispatch) => {
  dispatch({
    type: CLEAR_FULLSCREEN_PHOTO,
  });
};

export const stopLoading = () => (dispatch) => {
  dispatch({
    type: PHOTOS_LOADING_FINISHED,
  });
};

const extractPhotoInfo = (list) =>
  list.map(item => ({
      id: item.id,
      description: item.description,
      userName: item.user.name,
      uriSmall: item.urls.small,
      uriFull: item.urls.full,
    })
  );

